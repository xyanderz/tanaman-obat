package com.dicoding.listtanamanobat

object TanamanData {
    private val tanamanNames = arrayOf (
        "Jahe",
        "Jintan Hitam",
        "Kapulaga",
        "Kayu Manis",
        "Kencur",
        "Kunyit",
        "Temulawak"
    )

    private val tanamanDetails = arrayOf (
        "Menghangatkan diri di tengah udara dingin seperti sekarang ini bisa dilakukan dengan mengonsumsi minuman jahe hangat",
        "Obat herbal ini disebut-sebut digunakan oleh Nabi Muhammad SAW untuk pengobatan di zaman dulu.",
        "Tanaman rempah kapulaga sering dimanfaatkan sebagai bumbu masak untuk makanan timur tengah.",
        "Rempah lain yang biasanya bisa dikembangkan di rumah serta dipanen sendiri ialah kayu manis.",
        "Selain kunyit, kencur pun merupakan tanaman herbal yang kerap diubah menjadi jamu.",
        "Rempah kunyit kerap dijadikan pewarna alami untuk nasi tumpeng atau nasi kuning.",
        "Temu lawak (Curcuma xanthorrhiza) adalah tumbuhan obat yang tergolong dalam suku temu-temuan (Zingiberaceae)."
    )

    private val tanamanImage = intArrayOf (
        R.drawable.jahe,
        R.drawable.jintan_hitam,
        R.drawable.kapulaga,
        R.drawable.kayumanis,
        R.drawable.kencur,
        R.drawable.kunyit,
        R.drawable.temulawak
    )

    val listData: ArrayList<Tanaman>
        get() {
            val list = arrayListOf<Tanaman>()
            for (position in tanamanNames.indices) {
                val tanaman = Tanaman()
                tanaman.name = tanamanNames[position]
                tanaman.detail = tanamanDetails[position]
                tanaman.photo = tanamanImage[position]
                list.add(tanaman)
            }
            return list
        }
}