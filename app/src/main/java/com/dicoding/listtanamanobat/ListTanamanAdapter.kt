package com.dicoding.listtanamanobat

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.AdapterListUpdateCallback
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.item_baris_tanaman.view.*

class ListTanamanAdapter(val listTanaman: ArrayList<Tanaman>) : RecyclerView.Adapter<ListTanamanAdapter.ListViewHolder>() {
    private lateinit var onItemClickCallback: OnItemClickCallback

    fun setOnItemClickCallback(onItemClickCallback: OnItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ListViewHolder {
        val view: View = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.item_baris_tanaman, viewGroup, false)
        return ListViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listTanaman.size
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        val tanaman = listTanaman[position]

        Glide.with(holder.itemView.context)
            .load(tanaman.photo)
            .apply(RequestOptions().override(55, 55))
            .into(holder.imgPhoto)

        holder.tvName.text = tanaman.name
        holder.tvDetails.text = tanaman.detail

        holder.itemView.setOnClickListener {onItemClickCallback.onItemClicked(listTanaman[holder.adapterPosition]) }
    }

    inner class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvName: TextView = itemView.findViewById(R.id.tv_tanaman)
        var tvDetails: TextView = itemView.findViewById(R.id.tv_tanaman_detail)
        var imgPhoto: ImageView = itemView.findViewById(R.id.img_tanaman)
    }

    interface OnItemClickCallback {
        fun onItemClicked(data: Tanaman)
    }
}