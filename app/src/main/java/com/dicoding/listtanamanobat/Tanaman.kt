package com.dicoding.listtanamanobat

data class Tanaman(
    var name: String = "",
    var detail: String = "",
    var photo: Int = 0
)