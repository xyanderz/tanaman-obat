package com.dicoding.listtanamanobat

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_tanaman_detail.*

class TanamanDetail : AppCompatActivity(), View.OnClickListener {

    override fun onClick(v: View?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tanaman_detail)

        val textTanamanDetail: TextView = findViewById(R.id.tv_tanaman_detail)
        textTanamanDetail.setOnClickListener(this)

        val img = intent.getIntExtra(EXTRA_IMG, 0)
        val judul = intent.getStringExtra(EXTRA_JUDUL)
        val detail = intent.getStringExtra(EXTRA_DETAIL)
        tv_tanaman_detail.text
    }

    companion object {
        const val EXTRA_IMG = "extra_img"
        const val EXTRA_JUDUL = "extra_judul"
        const val EXTRA_DETAIL = "extra_detail"
    }
}
