package com.dicoding.listtanamanobat

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.item_baris_tanaman.*
import org.w3c.dom.Text

class MainActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var rvTanaman: RecyclerView
    private var list: ArrayList<Tanaman> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        rvTanaman = findViewById(R.id.rv_tanaman)
        rvTanaman.setHasFixedSize(true)

        list.addAll(TanamanData.listData)
        showRecyclerList()


    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.tv_tanaman -> {
                val moveIntent = Intent(this@MainActivity, TanamanDetail::class.java)
                moveIntent.putExtra(TanamanDetail.EXTRA_IMG, 0)
                moveIntent.putExtra(TanamanDetail.EXTRA_JUDUL, "")
                moveIntent.putExtra(TanamanDetail.EXTRA_DETAIL, "")
                startActivity(moveIntent)
            }
        }
    }

    private fun showRecyclerList() {
        rvTanaman.layoutManager = LinearLayoutManager(this)
        val listTanamanAdapter = ListTanamanAdapter(list)
        rvTanaman.adapter = listTanamanAdapter

        listTanamanAdapter.setOnItemClickCallback(object : ListTanamanAdapter.OnItemClickCallback {
            override fun onItemClicked(data: Tanaman) {
                showSelectedTanaman(data)
            }
        })
    }

    private fun showSelectedTanaman(tanaman: Tanaman) {
        startActivity(Intent(this,TanamanDetail::class.java))
        Toast.makeText(this, "Kamu memilih " + tanaman.name, Toast.LENGTH_SHORT).show()
    }
}